package GUI;

import Game.Board;
import Game.Game;
import Game.ChessUtils;

import javax.swing.*;
import java.awt.*;

public class GameGUI {

    public static void main(String[] args) {
        JFrame gameFrame = new JFrame("Java Chess Game");

        gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameFrame.setLayout(new BorderLayout());

        Board board = new Board();
        Game game = new Game(board);

        gameFrame.add(new BoardPanel(game, gameFrame, board), BorderLayout.CENTER);

        gameFrame.setSize(ChessUtils.GAME_FRAME_DIMENSION);
        gameFrame.setVisible(true);
    }
}
