package GUI;

import Game.*;
import Pieces.Piece;
import Game.ChessUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static Game.ChessUtils.darkTileColor;
import static Game.ChessUtils.lightTileColor;
import static javax.swing.SwingUtilities.isLeftMouseButton;
import static javax.swing.SwingUtilities.isRightMouseButton;

public class BoardPanel extends JPanel {
    private Game game;
    Coordinates sourceTile;
    Coordinates destinationTile;
    Piece humanMovedPiece;
    Piece capturedPiece;

    private List<TilePanel> boardTiles = new ArrayList<>();

    public BoardPanel(Game game, JFrame frame, Board board) {
        super(new GridLayout(8,8));

        this.game = game;

        for(int i = 0; i < ChessUtils.NUM_ROWS; i++) {
            for(int j = 0; j < ChessUtils.NUM_TILES_PER_ROW; j++) {
                TilePanel tilePanel = new TilePanel(new Coordinates(i,j), frame, board);
                boardTiles.add(tilePanel);
                add(tilePanel);
                setPreferredSize(ChessUtils.BOARD_PANEL_DIMENSION);
                validate();
            }
        }
    }

    public void drawBoard(Board board) {
        removeAll();
        for(TilePanel tilePanel : boardTiles) {
            tilePanel.drawTile(board);
            add(tilePanel);
        }
        validate();
        repaint();
    }

    public class TilePanel extends JPanel {
        Coordinates tileId;
        JFrame frame;

        TilePanel (Coordinates tileId, JFrame frame, Board board) {
            this.tileId = tileId;
            this.frame = frame;
            setPreferredSize(ChessUtils.TILE_PANEL_DIMENSION);
            assignTileColor();
            assignTilePieceIcon(board);
            this.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if(isRightMouseButton(e)) {
                        sourceTile = null;
                        destinationTile = null;
                        humanMovedPiece = null;
                        capturedPiece = null;
                    } else if(isLeftMouseButton(e)) {
                        if(sourceTile == null) {
                            sourceTile = tileId;
                            humanMovedPiece = board.getSquare(sourceTile).getPiece();
                            if(humanMovedPiece == null) {
                                sourceTile = null;
                            }
                        } else {
                            destinationTile = tileId;

                            Square source_square = board.getSquare(sourceTile);
                            Square destination_square = board.getSquare(destinationTile);

                            capturedPiece = destination_square.getPiece();

                            Move move = new Move(source_square, destination_square, humanMovedPiece, capturedPiece);
                            boolean moveMade = game.makeMove(move, false);

                            boolean gameOver;
                            if(moveMade) {
                                game.switchPlayer();
                                gameOver = game.isGameOver();
                                if(gameOver) {
                                    JOptionPane.showMessageDialog(frame, "White won");
                                    game.resetBoard();
                                }

                                boolean aiMoveMade = game.makeMove(null, true);
                                if(aiMoveMade) {
                                    game.switchPlayer();
                                    gameOver = game.isGameOver();
                                    if(gameOver) {
                                        JOptionPane.showMessageDialog(frame, "Black won");
                                        game.resetBoard();
                                    }

                                } else {
                                    JOptionPane.showMessageDialog(frame, "Game is over");
                                    game.resetBoard();
                                }
                                sourceTile = null;
                                destinationTile = null;
                                humanMovedPiece = null;
                            } else {
                                JOptionPane.showMessageDialog(frame, "That was a wrong move");
                                sourceTile = null;
                                destinationTile = null;
                                humanMovedPiece = null;
                            }

                        }

                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                drawBoard(board);
                            }
                        });
                    }

                }

                @Override
                public void mousePressed(MouseEvent e) {

                }

                @Override
                public void mouseReleased(MouseEvent e) {
                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            });
        }



        private void assignTileColor(){

            if(ChessUtils.UNEQUAL_ROW[tileId.getX()]) {
                setBackground(tileId.getY() % 2 != 0 ? lightTileColor : darkTileColor);
            } else {
                setBackground(tileId.getY() % 2 == 0 ? lightTileColor : darkTileColor);
            }

        }

        private void assignTilePieceIcon(Board board) {
            this.removeAll();
            if(board.getSquare(tileId).isOccupied()) {

                String al;
                if(board.getSquare(tileId).getPiece().getAlliance() == Alliance.WHITE) {
                    al = "W";
                } else {
                    al = "B";
                }


                try {
                    final BufferedImage image = ImageIO.read(new File("art/simple/" + al +
                            board.getSquare(tileId).getPiece().toString() + ".gif"));

                    add(new JLabel(new ImageIcon(image)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void drawTile (Board board) {
            assignTileColor();
            assignTilePieceIcon(board);
            validate();
            repaint();
        }
    }

}
