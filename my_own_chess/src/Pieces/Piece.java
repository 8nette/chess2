package Pieces;

import Game.Alliance;
import Game.Coordinates;

public abstract class Piece {
    private Alliance alliance;

    public Piece(Alliance alliance) {
        this.alliance = alliance;
    }

    public Alliance getAlliance() {
        return alliance;
    }

    public abstract PieceType getPieceType();

    public abstract boolean isValidMove(Coordinates initCoordinates, Coordinates finalCoordinates);

    public abstract Coordinates[] drawPath(Coordinates initCoordinates, Coordinates finalCoordinates);

    public abstract String toString();

    public boolean equals(Piece other) {
        if(this == null && other == null) {
            return true;
        } else if (this == null || other == null) {
            return false;
        } else if(this.getPieceType() == other.getPieceType() && this.getAlliance() == other.getAlliance()) {
            return true;
        }
        return false;
    }
}
