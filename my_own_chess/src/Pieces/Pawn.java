package Pieces;

import Game.Alliance;
import Game.Coordinates;

public class Pawn extends Piece{
    public Pawn(Alliance alliance) {
        super(alliance);
    }

    @Override
    public PieceType getPieceType() {
        return PieceType.PAWN;
    }

    @Override
    public boolean isValidMove(Coordinates initCoordinates, Coordinates finalCoordinates) {
        boolean white_alliance = getAlliance() == Alliance.WHITE;
        boolean black_alliance = getAlliance() == Alliance.BLACK;
        boolean backwards = initCoordinates.getY() > finalCoordinates.getY();
        boolean forwards = initCoordinates.getY() < finalCoordinates.getY();

        boolean white_and_backwards = white_alliance && backwards;
        boolean black_and_forwards = black_alliance && forwards;

        if(white_and_backwards || black_and_forwards) {
            return false;
        }

        int x_diff = Math.abs(initCoordinates.getX() - finalCoordinates.getX());
        int y_diff = Math.abs(initCoordinates.getY() - finalCoordinates.getY());

        boolean one_square = x_diff == 0 && y_diff == 1;
        boolean two_square = x_diff == 0 && y_diff == 2;
        boolean capture = x_diff == 1 && y_diff == 1;

        return one_square || two_square || capture;
    }

    @Override
    public Coordinates[] drawPath(Coordinates initCoordinates, Coordinates finalCoordinates) {
        if(initCoordinates.getX() != finalCoordinates.getX()) {
            return new Coordinates[]{initCoordinates, finalCoordinates};
        }

        int length = Math.abs(initCoordinates.getY() - finalCoordinates.getY()) + 1;
        Coordinates[] path = new Coordinates[length + 1];

        int y_direction = 1;

        if (finalCoordinates.getY() - initCoordinates.getY() < 0) {
            y_direction = -1;
        }

        for (int i = 0; i < length + 1; i++) {
            path[i] = new Coordinates(initCoordinates.getX(), initCoordinates.getY() + y_direction*(i));
        }

        return path;
    }

    public String toString() {return "P";}
}
