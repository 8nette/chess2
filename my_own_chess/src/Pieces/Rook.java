package Pieces;

import Game.Alliance;
import Game.Coordinates;

public class Rook extends Piece{
    public Rook(Alliance alliance) {
        super(alliance);
    }

    @Override
    public PieceType getPieceType() {
        return PieceType.ROOK;
    }

    @Override
    public boolean isValidMove(Coordinates initCoordinates, Coordinates finalCoordinates) {
        int x_diff = Math.abs(finalCoordinates.getX() - initCoordinates.getX());
        int y_diff = Math.abs(finalCoordinates.getY() - initCoordinates.getY());

        return x_diff == 0 || y_diff == 0;
    }

    @Override
    public Coordinates[] drawPath(Coordinates initCoordinates, Coordinates finalCoordinates) {

        int x_diff = Math.abs(finalCoordinates.getX() - initCoordinates.getX());
        int length = Math.abs(initCoordinates.getX() - finalCoordinates.getX()) +
                Math.abs(initCoordinates.getY() - finalCoordinates.getY());

        int x_direction = 1;
        int y_direction = 1;

        if(finalCoordinates.getX() - initCoordinates.getX() < 0) {
            x_direction = -1;
        }
        if (finalCoordinates.getY() - initCoordinates.getY() < 0) {
            y_direction = -1;
        }

        Coordinates[] path = new Coordinates[length+1];

        for (int i = 0; i < length + 1; i++) {
            if(x_diff == 0){
                path[i] = new Coordinates(initCoordinates.getX(), initCoordinates.getY() + y_direction*i);
            } else {
                path[i] = new Coordinates(initCoordinates.getX() + x_direction*i, initCoordinates.getY());
            }
        }

        return path;
    }

    public String toString() {return "R";}
}
