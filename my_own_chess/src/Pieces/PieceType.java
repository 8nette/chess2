package Pieces;

public enum PieceType {
    BISHOP, KING, KNIGHT, PAWN, QUEEN, ROOK
}
