package Pieces;

import Game.Alliance;
import Game.Coordinates;

public class Knight extends Piece{
    public Knight(Alliance alliance) {
        super(alliance);
    }

    @Override
    public PieceType getPieceType() {
        return PieceType.KNIGHT;
    }

    @Override
    public boolean isValidMove(Coordinates initCoordinates, Coordinates finalCoordinates) {
        int x_diff = Math.abs(initCoordinates.getX() - finalCoordinates.getX());
        int y_diff = Math.abs(initCoordinates.getY() - finalCoordinates.getY());

        return x_diff + y_diff == 3 && x_diff != 0 && y_diff != 0;
    }

    @Override
    public Coordinates[] drawPath(Coordinates initCoordinates, Coordinates finalCoordinates) {
        //The knight can jump over pieces.
        return new Coordinates[]{initCoordinates,finalCoordinates};
    }

    public String toString() {return "N";}
}
