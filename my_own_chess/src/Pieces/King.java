package Pieces;

import Game.Alliance;
import Game.Coordinates;

public class King extends Piece{
    public King(Alliance alliance) {
        super(alliance);
    }

    @Override
    public PieceType getPieceType() {
        return PieceType.KING;
    }

    @Override
    public boolean isValidMove(Coordinates initCoordinates, Coordinates finalCoordinates) {
        int x_diff = Math.abs(finalCoordinates.getX() - initCoordinates.getX());
        int y_diff = Math.abs(finalCoordinates.getY() - initCoordinates.getY());

        boolean x_zero_or_one = x_diff == 0 || x_diff == 1;
        boolean y_zero_or_one = y_diff == 0 || y_diff == 1;
        boolean zero = x_diff == 0 && y_diff == 0;

        return x_zero_or_one && y_zero_or_one && !zero;
    }

    @Override
    public Coordinates[] drawPath(Coordinates initCoordinates, Coordinates finalCoordinates) {
        return new Coordinates[]{initCoordinates, finalCoordinates};
    }

    public String toString() {return "K";}
}
