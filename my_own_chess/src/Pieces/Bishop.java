package Pieces;

import Game.Alliance;
import Game.ChessUtils;
import Game.Coordinates;

public class Bishop extends Piece{
    public Bishop(Alliance alliance) {
        super(alliance);
    }

    @Override
    public PieceType getPieceType() {
        return PieceType.BISHOP;
    }

    @Override
    public boolean isValidMove(Coordinates initCoordinates, Coordinates finalCoordinates) {
        int x_diff = Math.abs(finalCoordinates.getX() - initCoordinates.getX());
        int y_diff = Math.abs(finalCoordinates.getY() - initCoordinates.getY());

        return x_diff == y_diff;
    }

    @Override
    public Coordinates[] drawPath(Coordinates initCoordinates, Coordinates finalCoordinates) {
        int length = Math.abs(initCoordinates.getX() - finalCoordinates.getX());

        int x_direction = 1;
        int y_direction = 1;

        if(finalCoordinates.getX() - initCoordinates.getX() < 0) {
            x_direction = -1;
        }
        if (finalCoordinates.getY() - initCoordinates.getY() < 0) {
            y_direction = -1;
        }

        Coordinates[] path = new Coordinates[length + 1];

        if(length > 0) {
            for (int i = 0; i < length + 1; i++) {
                path[i] = new Coordinates(initCoordinates.getX() + x_direction*(i),
                        initCoordinates.getY() + y_direction*(i));
            }
        }

        return path;
    }

    public String toString() {return "B";}
}
