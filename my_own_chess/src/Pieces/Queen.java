package Pieces;

import Game.Alliance;
import Game.Coordinates;

public class Queen extends Piece{
    public Queen(Alliance alliance) {
        super(alliance);
    }

    @Override
    public PieceType getPieceType() {
        return PieceType.QUEEN;
    }

    @Override
    public boolean isValidMove(Coordinates initCoordinates, Coordinates finalCoordinates) {
        int x_diff = Math.abs(finalCoordinates.getX() - initCoordinates.getX());
        int y_diff = Math.abs(finalCoordinates.getY() - initCoordinates.getY());

        return x_diff == y_diff || x_diff == 0 || y_diff == 0;
    }

    @Override
    public Coordinates[] drawPath(Coordinates initCoordinates, Coordinates finalCoordinates) {
        int length_x = Math.abs(initCoordinates.getX() - finalCoordinates.getX());
        int length_y = Math.abs(initCoordinates.getY() - finalCoordinates.getY());

        int rook_length = Math.abs(initCoordinates.getX() - finalCoordinates.getX()) +
                Math.abs(initCoordinates.getY() - finalCoordinates.getY());

        int x_direction = 1;
        int y_direction = 1;

        if(finalCoordinates.getX() - initCoordinates.getX() < 0) {
            x_direction = -1;
        }
        if (finalCoordinates.getY() - initCoordinates.getY() < 0) {
            y_direction = -1;
        }

        Coordinates[] path;

        //bishop move
        if(length_x == length_y) {
            path = new Coordinates[length_x + 1];

            for (int i = 0; i < length_x + 1; i++) {
                path[i] = new Coordinates(initCoordinates.getX() + x_direction*(i),
                        initCoordinates.getY() + y_direction*(i));
            }
            //rook move
        } else {
            path = new Coordinates[rook_length + 1];

            for (int i = 0; i < rook_length + 1; i++) {
                if(length_x == 0){
                    path[i] = new Coordinates(initCoordinates.getX(), initCoordinates.getY() + y_direction*i);
                } else {
                    path[i] = new Coordinates(initCoordinates.getX() + x_direction*i, initCoordinates.getY());
                }
            }
        }

        return path;
    }

    public String toString() {return "Q";}
}
