package Game;

import java.awt.*;

public class ChessUtils {
    public static final int NUM_TILES_PER_ROW = 8;
    public static final int NUM_ROWS = 8;

    public final static Dimension GAME_FRAME_DIMENSION = new Dimension(600, 600);
    public final static Dimension BOARD_PANEL_DIMENSION = new Dimension(400, 350);
    public final static Dimension TILE_PANEL_DIMENSION = new Dimension(10, 10);

    public final static Color lightTileColor = new Color(250, 230, 200);
    public final static Color darkTileColor = new Color(60, 50, 40);

    public static final int global_depth = 6;

    public static final boolean[] UNEQUAL_ROW = {false, true, false, true, false, true, false, true};

    private ChessUtils(){
        throw new RuntimeException("You cannot instantiate me!");
    }

}
