package Game;

public class Game {
    Board board;
    Alliance currentPlayer;
    Player blackPlayer;
    Player whitePlayer;


    public Game(Board board) {
        this.board = board;
        this.currentPlayer = Alliance.WHITE;
        this.blackPlayer = new Player(board, Alliance.BLACK, this);
        this.whitePlayer = new Player(board, Alliance.WHITE, this);
    }

    public Board getBoard() {
        return board;
    }

    public void switchPlayer() {
        if(currentPlayer == Alliance.WHITE) {
            currentPlayer = Alliance.BLACK;
        } else {
            currentPlayer = Alliance.WHITE;
        }
    }

    public void resetBoard() {
        board.resetBoard();
        currentPlayer = Alliance.WHITE;
    }

    public boolean isGameOver() {
        boolean checkMate;
        if(currentPlayer == Alliance.WHITE) {
            checkMate = whitePlayer.isCheckMate();
        } else {
            checkMate = blackPlayer.isCheckMate();
        }

        return checkMate;
    }

    public boolean makeMove(Move move, boolean AI) {
        boolean moveMade;
        if(currentPlayer == Alliance.WHITE) {
            if(AI) {
                moveMade = whitePlayer.AIMove();
            } else {
                moveMade = whitePlayer.makeMove(move);
            }
        } else {
            if(AI) {
                moveMade = blackPlayer.AIMove();
            } else {
                moveMade = blackPlayer.makeMove(move);
            }
        }

        if(moveMade) {
            return true;
        }
        return false;
    }

}
