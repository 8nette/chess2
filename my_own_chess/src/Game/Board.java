package Game;

import Pieces.*;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.exit;

public class Board {
    private Square[][] board = new Square[8][8];
    private List<Move> moveList = new ArrayList<>();

    public Board() {
        setSquares();
        setWhitePieces();
        setBlackPieces();
    }

    public void resetBoard() {
        setSquares();
        setWhitePieces();
        setBlackPieces();
    }

    public Square[][] getBoard() {
        return board;
    }

    public Square getSquare(Coordinates coordinates) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if(board[i][j].getCoordinates().equals(coordinates)) {
                    return board[i][j];
                }
            }
        }
        return null;
    }

    public Square[][] getSquares() {
        return board;
    }

    public List<Move> getMoveList() {
        return moveList;
    }

    public Move getLastMove() {
        if(moveList.size() >= 1){
            return moveList.get(moveList.size() - 1);
        } else {
            return null;
        }
    }

    public void makeMove(Square initSquare, Square finalSquare) {
        Piece capturedPiece = null;
        if(finalSquare.isOccupied()) {
            capturedPiece = finalSquare.getPiece();
            finalSquare.releasePiece();
        }
        finalSquare.setPiece(initSquare.getPiece());
        initSquare.releasePiece();

        moveList.add(new Move(initSquare, finalSquare, initSquare.getPiece(), capturedPiece));
        System.out.println("move was made");
    }

    public void undoMove(Move move) {
        if(move.getCapturedPiece() != null) {
            move.getFinalSquare().setPiece(move.getCapturedPiece());
        } else {
            move.getFinalSquare().releasePiece();
        }
        move.getInitSquare().setPiece(move.getMovingPiece());

        moveList.remove(move);
        moveList.add(new Move(move.getFinalSquare(), move.getInitSquare(), move.getMovingPiece(), null));
        System.out.println("move was undone");
    }

    private void setSquares() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                board[i][j] = new Square(new Coordinates(i,j), null);
            }
        }
    }

    private void setWhitePieces() {
        board[0][0].setPiece(new Rook(Alliance.WHITE));
        board[1][0].setPiece(new Knight(Alliance.WHITE));
        board[2][0].setPiece(new Bishop(Alliance.WHITE));
        board[3][0].setPiece(new Queen(Alliance.WHITE));
        board[4][0].setPiece(new King(Alliance.WHITE));
        board[5][0].setPiece(new Bishop(Alliance.WHITE));
        board[6][0].setPiece(new Knight(Alliance.WHITE));
        board[7][0].setPiece(new Rook(Alliance.WHITE));

        board[0][1].setPiece(new Pawn(Alliance.WHITE));
        board[1][1].setPiece(new Pawn(Alliance.WHITE));
        board[2][1].setPiece(new Pawn(Alliance.WHITE));
        board[3][1].setPiece(new Pawn(Alliance.WHITE));
        board[4][1].setPiece(new Pawn(Alliance.WHITE));
        board[5][1].setPiece(new Pawn(Alliance.WHITE));
        board[6][1].setPiece(new Pawn(Alliance.WHITE));
        board[7][1].setPiece(new Pawn(Alliance.WHITE));
    }

    private void setBlackPieces() {
        board[0][7].setPiece(new Rook(Alliance.BLACK));
        board[1][7].setPiece(new Knight(Alliance.BLACK));
        board[2][7].setPiece(new Bishop(Alliance.BLACK));
        board[3][7].setPiece(new Queen(Alliance.BLACK));
        board[4][7].setPiece(new King(Alliance.BLACK));
        board[5][7].setPiece(new Bishop(Alliance.BLACK));
        board[6][7].setPiece(new Knight(Alliance.BLACK));
        board[7][7].setPiece(new Rook(Alliance.BLACK));

        board[0][6].setPiece(new Pawn(Alliance.BLACK));
        board[1][6].setPiece(new Pawn(Alliance.BLACK));
        board[2][6].setPiece(new Pawn(Alliance.BLACK));
        board[3][6].setPiece(new Pawn(Alliance.BLACK));
        board[4][6].setPiece(new Pawn(Alliance.BLACK));
        board[5][6].setPiece(new Pawn(Alliance.BLACK));
        board[6][6].setPiece(new Pawn(Alliance.BLACK));
        board[7][6].setPiece(new Pawn(Alliance.BLACK));
    }
}
