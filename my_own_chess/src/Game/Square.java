package Game;

import Pieces.Piece;

public class Square {
    private final Coordinates coordinates;
    private Piece piece;

    public Square(final Coordinates coordinates, Piece piece) {
        this.coordinates = coordinates;
        this.piece = piece;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public boolean isOccupied() {
        if(piece == null) {
            return false;
        }
        return true;
    }

    public boolean equals(Square square) {
        if(square.getCoordinates().equals(coordinates)) {
            return true;
        }
        return false;
    }

    public void releasePiece() {
        piece = null;
    }
}
