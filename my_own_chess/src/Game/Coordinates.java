package Game;

public class Coordinates {
    private final int x;
    private final int y;

    public Coordinates(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isValid() {
        if(x >=0 && x < 8 && y >= 0 && y < 8) {
            return true;
        }
        return false;
    }

    public boolean equals(Coordinates coordinates) {
        if(coordinates.getX() == x && coordinates.getY() == y) {
            return true;
        }
        return false;
    }
}
