package Game;

import Pieces.Piece;

public class Move {
    private Square initSquare;
    private Square finalSquare;
    private Piece movingPiece;
    private Piece capturedPiece;

    public Move(Square initSquare, Square finalSquare, Piece movingPiece, Piece capturedPiece) {
        this.initSquare = initSquare;
        this.finalSquare = finalSquare;
        this.movingPiece = movingPiece;
        this.capturedPiece = capturedPiece;
    }

    public Square getInitSquare() { return initSquare; }

    public Square getFinalSquare() { return finalSquare; }

    public Piece getMovingPiece() { return movingPiece; }

    public Piece getCapturedPiece() { return capturedPiece; }

    public boolean equals(Move other) {
        Coordinates this_init_coordinates = this.initSquare.getCoordinates();
        Coordinates this_final_coordinates = this.finalSquare.getCoordinates();

        Coordinates other_init_coordinates = other.getInitSquare().getCoordinates();
        Coordinates other_final_coordinates = other.getFinalSquare().getCoordinates();

        boolean init_bool = this_init_coordinates.equals(other_init_coordinates);
        boolean final_bool = this_final_coordinates.equals(other_final_coordinates);

        Piece this_moving_piece = this.movingPiece;
        Piece this_captured_piece = this.capturedPiece;

        Piece other_moving_piece = other.getMovingPiece();
        Piece other_captured_piece = other.getCapturedPiece();

        boolean moving_bool;
        boolean captured_bool;

        if(this_moving_piece != null) {
            moving_bool = this_moving_piece.equals(other_moving_piece);
        } else if (this_moving_piece == null && other_moving_piece == null) {
            moving_bool = true;
        } else { moving_bool = false; }

        if(this_captured_piece != null) {
            captured_bool = this_captured_piece.equals(other_captured_piece);
        } else if (this_captured_piece == null && other_captured_piece == null) {
            captured_bool = true;
        } else { captured_bool = false; }

        return init_bool && final_bool && moving_bool && captured_bool;
    }


}
