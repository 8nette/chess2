package Game;

import Pieces.*;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CheckedOutputStream;

import static java.lang.System.exit;

public class Player {
    private Alliance alliance;
    private Board board;
    private Game game;

    public Player(Board board, Alliance alliance, Game game){
        this.alliance = alliance;
        this.board = board;
        this.game = game;
    }

    public Alliance getAlliance() {
        return alliance;
    }

    public boolean makeMove(Move move) {
        if(isValidEnpassant(move.getInitSquare(), move.getFinalSquare())) {
            if(isValidEnpassantMove(move)){
                makeEnpassantMove(move.getInitSquare(), move.getFinalSquare());
            }
        } else if(isValidMove(move)){
            board.makeMove(move.getInitSquare(), move.getFinalSquare());
            return true;
        }
        return false;
    }



    public void undoMove(Move move) {
        board.undoMove(move);
    }

    private boolean isValidMove(Move move) {
        if(isSaneMove(move) && isLegalMove(move)) {
            return true;
        }
        return false;
    }

    private boolean isLegalMove(Move move){
        if(!isSaneMove(move)) {
            return false;
        }

        Coordinates[] path = move.getMovingPiece().drawPath(move.getInitSquare().getCoordinates(), move.getFinalSquare().getCoordinates());
        for (int i = 0; i < path.length - 1; i++) {
            if(board.getSquare(path[i]) != null) {
                if(i != 0 && i != path.length - 1) {
                    if(board.getSquare(path[i]).isOccupied()) {
                        return false;
                    }
                }
            }
        }

        if(!move.getMovingPiece().isValidMove(move.getInitSquare().getCoordinates(), move.getFinalSquare().getCoordinates())) {
            return false;
        }

        if(move.getMovingPiece().getPieceType() == PieceType.PAWN) {
            if(isPawnCapture(move)) {
                if(!isValidPawnCapture(move)) {
                    return false;
                }
            }
        }

        if(move.getMovingPiece().getPieceType() == PieceType.PAWN) {
            if(!isPawnCapture(move)) {
                if(move.getFinalSquare().isOccupied()) {
                    return false;
                }
            }
        }

        if(isValidEnpassant(move.getInitSquare(), move.getFinalSquare())) {
            if(!isValidEnpassantMove(move)) {
                return false;
            }
        }

        return true;
    }

    private boolean isSaneMove(Move move) {
        Square initSquare = move.getInitSquare();
        Square finalSquare = move.getFinalSquare();

        if(initSquare == null || finalSquare == null) {
            return false;
        }
        if(!initSquare.getCoordinates().isValid() || !finalSquare.getCoordinates().isValid()) {
            return false;
        }
        if(!initSquare.isOccupied()) {
            return false;
        }
        if(initSquare.equals(finalSquare)) {
            return false;
        }
        if(move.getMovingPiece() == null) {
            return false;
        }
        if(finalSquare.isOccupied()) {
            if(finalSquare.getPiece().getAlliance() == alliance) {
                return false;
            }
        }
        if(initSquare.isOccupied()) {
            if(initSquare.getPiece().getAlliance() != alliance) {
                return false;
            }
        }


        return true;
    }

    //-------------------------------------------------------------------------------------------------------//
    //valid pawn capture

    public boolean isPawnCapture(Move move) {
        Coordinates initCoordinates = move.getInitSquare().getCoordinates();
        Coordinates finalCoordinates = move.getFinalSquare().getCoordinates();

        int x_diff = Math.abs(initCoordinates.getX() - finalCoordinates.getX());
        int y_diff = Math.abs(initCoordinates.getY() - finalCoordinates.getY());

        return x_diff == 1 && y_diff == 1;
    }

    private boolean isValidPawnCapture(Move move) {
        if(!isPawnCapture(move) && move.getFinalSquare().isOccupied()) {
            return false;
        }

        Square initSquare = move.getInitSquare();
        Square finalSquare = move.getFinalSquare();

        if (!finalSquare.isOccupied()
                || initSquare.getPiece().getPieceType() != PieceType.PAWN) {
            return false;
        }
        Coordinates initPos = initSquare.getCoordinates();
        Coordinates finalPos = finalSquare.getCoordinates();
        Alliance initAlliance = initSquare.getPiece().getAlliance();

        // This is for normal pawn capture moves.
        if (Math.abs(initPos.getY() - finalPos.getY()) == 1
                && Math.abs(initPos.getX() - finalPos.getX()) == 1) {
            // White can only move forward
            if (initAlliance == Alliance.WHITE) {
                if (initPos.getY() < finalPos.getY()) {
                    return true;
                }
            }
            // Black can only move backward in a sense.
            if (initAlliance == Alliance.BLACK.BLACK) {
                if (initPos.getY() > finalPos.getY()) {
                    return true;
                }
            }

        }
        return false;

    }

    //--------------------------------------------------------------------------------------------------------//
    //valid enpassant move

    private void makeEnpassantMove(Square initSquare, Square finalSquare) {
        Move lastMove = board.getMoveList().get(board.getMoveList().size() - 1);
        lastMove.getFinalSquare().releasePiece();
        board.makeMove(initSquare, finalSquare);
    }

    private boolean isValidEnpassant(Square initSquare, Square finalSquare) {
        // The final square should be empty
        if (finalSquare.isOccupied()) {
            return false;
        }

        // The first piece should be a pawn.
        if (initSquare.getPiece().getPieceType() != PieceType.PAWN) {
            return false;
        }
        // Move type is different according to player color
        if (initSquare.getPiece().getAlliance() == Alliance.WHITE) {
            if (initSquare.getCoordinates().getY() > finalSquare.getCoordinates().getY()) {
                // White can only move forward
                return false;
            }
        } else {
            if (initSquare.getCoordinates().getY() < finalSquare.getCoordinates().getY()) {
                // Black can only move backward
                return false;
            }
        }
        // The move should be like a bishop move to a single square.
        if (Math.abs(initSquare.getCoordinates().getX() - finalSquare.getCoordinates().getX()) == 1
                && Math.abs(initSquare.getCoordinates().getY()
                - finalSquare.getCoordinates().getY()) == 1) {
            // There should be a pawn move before enpassant.
            if (board.getMoveList().isEmpty()) {
                return false;
            }
            Move lastMove = board.getMoveList().get(board.getMoveList().size() - 1);
            if (lastMove.getMovingPiece() == null) {
                return false;
            }

            if (lastMove.getMovingPiece().getPieceType() == PieceType.PAWN) {
                // The pawn should be moving two steps forward/backward.
                // And our pawn should be moving to the same file as the last
                // pawn
                if (Math.abs(lastMove.getFinalSquare().getCoordinates().getY()
                        - lastMove.getInitSquare().getCoordinates().getY()) == 2
                        && lastMove.getFinalSquare().getCoordinates().getX() == finalSquare
                        .getCoordinates().getX()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isValidEnpassantMove(Move move) {
        if(!isValidEnpassant(move.getInitSquare(), move.getFinalSquare())) {
            return false;
        }
        if(move.getFinalSquare().isOccupied()) {
            return false;
        }
        if(move.getMovingPiece().getPieceType() != PieceType.PAWN) {
            return false;
        }
        if(move.getMovingPiece().getAlliance() == Alliance.WHITE) {
            if(move.getInitSquare().getCoordinates().getY() > move.getFinalSquare().getCoordinates().getY()) {
                return false;
            }
        } else {
            if(move.getInitSquare().getCoordinates().getY() < move.getFinalSquare().getCoordinates().getY()) {
                return false;
            }
        }

        Coordinates initCoordinates = move.getInitSquare().getCoordinates();
        Coordinates finalCoordinates = move.getFinalSquare().getCoordinates();

        int x_diff = Math.abs(initCoordinates.getX() - finalCoordinates.getX());
        int y_diff = Math.abs(initCoordinates.getY() - finalCoordinates.getY());

        if(x_diff == 1 && y_diff == 1) {
            List<Move> moveList = board.getMoveList();
            if(moveList.isEmpty()) {
                return false;
            }
            Move lastMove = board.getLastMove();
            if(lastMove.getMovingPiece() == null) {
                return false;
            }
            if(lastMove.getMovingPiece().getPieceType() == PieceType.PAWN) {
                int lastMove_init_y = lastMove.getInitSquare().getCoordinates().getY();
                int lastMove_final_y = lastMove.getFinalSquare().getCoordinates().getY();
                int lastMove_y_diff = Math.abs(lastMove_final_y - lastMove_init_y);

                if(lastMove_y_diff == 2) {
                    return true;
                }
            }

        }
        return false;
    }

    //--------------------------------------------------------------------------------------------------------//
    //valid castle move
    private boolean isValidCastleMove(Square kingSquare, Square rookSquare) {
        // Check if the squares are occupied.
        if (!(kingSquare.isOccupied() && rookSquare.isOccupied())) {
            return false;
        }
        // Check if the pieces have been moved or not.
        if (hasPieceMoved(kingSquare) || hasPieceMoved(rookSquare)) {
            return false;
        }

        // First check if the move is valid.
        if (!rookSquare.getPiece().isValidMove(kingSquare.getCoordinates(),
                rookSquare.getCoordinates())) {
            return false;
        }
        // Check if the path is clear
        if (!isPathClear(
                rookSquare.getPiece().drawPath(rookSquare.getCoordinates(),
                        kingSquare.getCoordinates()),
                rookSquare.getCoordinates(), kingSquare.getCoordinates())) {
            return false;
        }
        // Now check if the movement of the castling is fine
        // First check if the piece is king and rook
        if (kingSquare.getPiece().getPieceType() == PieceType.KING
                && rookSquare.getPiece().getPieceType() == PieceType.ROOK) {

            int col = 0;
            if (kingSquare.getPiece().getAlliance() == Alliance.BLACK) {
                col = 7;
            }
            // The peices are in correct position for castling.

            if (kingSquare.getCoordinates().equals(new Coordinates(4, col))
                    && (rookSquare.getCoordinates().equals(
                    new Coordinates(0, col)) || rookSquare
                    .getCoordinates().equals(new Coordinates(7, col)))) {

                // Check if there is check in any way between the king and final
                // king square
                int offset;
                if (Math.signum(rookSquare.getCoordinates().getX()
                        - kingSquare.getCoordinates().getX()) == 1) {
                    offset = 2;
                } else {
                    offset = -2;
                }
            }
        }
        return false;
    }

    private boolean hasPieceMoved(Square square) {
        for (Move move : board.getMoveList()) {
            if (move.getInitSquare().equals(square)
                    || move.getFinalSquare().equals(square)) {
                return true;
            }
        }
        return false;
    }

    private boolean isPathClear(Coordinates[] path, Coordinates initCoordinate,
                                Coordinates finalCoordinate) {
        Square[][] squares = board.getBoard();

        for (Coordinates coordinate : path) {

            if(coordinate.getX() == 0) {
                return false;
            }

            if ((squares[coordinate.getX()][coordinate.getY()].isOccupied())
                    && (!coordinate.equals(initCoordinate))
                    && (!coordinate.equals(finalCoordinate))) {
                return false;
            }
        }
        return true;
    }

    //--------------------------------------------------------------------------------------------------------//
    public boolean isCheckMate() {
        if(isKingSafe()) {
            return false;
        }

        Square[] attackers = getAttackingPieces();
        if(attackers.length == 0) {
            return false;
        }

        boolean checkmate = true;
        Square attackerSquare = attackers[0];
        Square kingSquare = squareOfKing();

        Coordinates[] attackPath = attackerSquare.getPiece().drawPath(attackerSquare.getCoordinates(), kingSquare.getCoordinates());

        Square[][] allSquares = board.getBoard();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Square tmpSquare = allSquares[i][j];
                //TODO: der mangler en masse hvis'er her!!!!
                if(kingSquare.getPiece().isValidMove(kingSquare.getCoordinates(), tmpSquare.getCoordinates())){
                    if(kingSquare != tmpSquare) {
                        checkmate = false;
                    }
                }

                if(tmpSquare.isOccupied()) {
                    for(Coordinates coordinates : attackPath) {
                        if(tmpSquare.getPiece().getAlliance() == kingSquare.getPiece().getAlliance()) {
                            if(tmpSquare.getPiece().isValidMove(tmpSquare.getCoordinates(), coordinates)) {
                                checkmate = false;
                            }
                        }
                    }
                }

            }
        }

        return checkmate;
    }

    private Square[] getAttackingPieces() {
        List<Square> squares = new ArrayList<>();
        Square[][] allSquares = board.getBoard();
        Square kingSquare = squareOfKing();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Square tmp = allSquares[i][j];
                if(tmp.isOccupied()) {
                    //TODO: this should never happen, some kind of win message should happen, when a king is gone
                    if(kingSquare == null) {
                        exit(0);
                    }
                    if(tmp.getPiece().isValidMove(tmp.getCoordinates(), kingSquare.getCoordinates())) {
                        if(tmp.getPiece().getAlliance() != kingSquare.getPiece().getAlliance()) {
                            squares.add(tmp);
                        }
                    }
                }
            }
        }

        return squares.toArray(new Square[0]);
    }

    private boolean isKingSafe() {
        if(getAttackingPieces().length > 0) {
            return false;
        } else {
            return true;
        }
    }

    //-----------------------------------------------------------------------------------------------------//

    public boolean AIMove() {
        RatingMove ratingMove = alphaBeta(ChessUtils.global_depth, 1000000, -1000000, null, 0, alliance);
        Move move = ratingMove.getMove();

        if(move != null) {
            makeMove(move);
            return true;
        } else {
            return false;
        }
    }

    private RatingMove alphaBeta(int depth, int beta, int alpha, Move move, int rating, Alliance alliance) {
        List<Move> moves = possibleMoves();

        if(depth == 0 || moves.size() == 0) {
            return new RatingMove(rating(moves.size(), depth, alliance, move), move);
        }

        for(Move potential_move : moves) {
            if(isValidMove(potential_move)) {
                makeMove(potential_move);
                if(alliance == Alliance.WHITE) {
                    alliance = Alliance.BLACK;
                } else {
                    alliance = Alliance.WHITE;
                }

                RatingMove ratingMove = alphaBeta(depth-1, beta, alpha, potential_move, rating, alliance);
                int returnRating = ratingMove.getRating();
                Move returnMove = ratingMove.getMove();

                undoMove(potential_move);

                if(alliance == Alliance.WHITE) {
                    if(returnRating <= beta) {
                        beta = returnRating;
                        if(depth == ChessUtils.global_depth) {
                            move = returnMove;
                        }
                    }
                } else {
                    if(returnRating > alpha) {
                        alpha = returnRating;
                        if(depth == ChessUtils.global_depth) {
                            move = returnMove;
                        }
                    }
                }

                if(alpha >= beta) {
                    if(alliance == Alliance.WHITE) {
                        return new RatingMove(beta, move);
                    } else {
                        return new RatingMove(alpha, move);
                    }
                }
            }
        }

        if(alliance == Alliance.WHITE) {
            return new RatingMove(beta, move);
        } else {
            return new RatingMove(alpha, move);
        }
    }

    private List<Move> possibleMoves() {
        List<Square> squares_with_right_alliance = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Square square = board.getSquare(new Coordinates(i,j));
                if(square.isOccupied()) {
                    if(square.getPiece().getAlliance() == alliance) {
                        squares_with_right_alliance.add(square);
                    }
                }
            }
        }

        List<Move> moves = new ArrayList<>();
        for(Square init_square : squares_with_right_alliance) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    Square final_square = board.getSquare(new Coordinates(i,j));
                    Move move = new Move(init_square, final_square, init_square.getPiece(), final_square.getPiece());
                    if(isValidMove(move)){
                        moves.add(move);
                    }
                }
            }
        }

        return moves;
    }

    private int rating(int listLength, int depth, Alliance alliance, Move move) {
        int rateMaterial = rateMaterial();

        int counter = 0;
        counter += rateMaterial();
        counter += rateAttack();
        counter += rateMovability(listLength, depth, rateMaterial, move);

        return counter;
    }

    private int rateMaterial() {
        int counter = 0;

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Square square = board.getSquare(new Coordinates(i, j));
                if(square.isOccupied() && square.getPiece().getAlliance() == alliance) {
                    if(square.getPiece().getPieceType() == PieceType.PAWN) {
                        counter += 100;
                    } else if (square.getPiece().getPieceType() == PieceType.ROOK) {
                        counter += 500;
                    } else if (square.getPiece().getPieceType() == PieceType.KNIGHT) {
                        counter += 300;
                    } else if (square.getPiece().getPieceType() == PieceType.BISHOP) {
                        counter += 300;
                    } else if (square.getPiece().getPieceType() == PieceType.QUEEN) {
                        counter += 900;
                    }
                }
            }
        }

        return counter;
    }

    private int rateAttack() {
        int counter = 0;
        Square kingSquare = squareOfKing();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Square square = board.getSquare(new Coordinates(i,j));
                if(square.isOccupied()) {
                    if(square.getPiece().getAlliance() != alliance) {
                        boolean kingSafe = kingSafe(new Move(square, kingSquare, square.getPiece(), kingSquare.getPiece()));
                        if(square.getPiece().getPieceType() == PieceType.PAWN && !kingSafe) {
                            counter -= -64;
                        } else if (square.getPiece().getPieceType() == PieceType.ROOK && !kingSafe) {
                            counter -= 500;
                        } else if (square.getPiece().getPieceType() == PieceType.KNIGHT && !kingSafe) {
                            counter -= 300;
                        } else if (square.getPiece().getPieceType() == PieceType.BISHOP && !kingSafe) {
                            counter -= 300;
                        } else if (square.getPiece().getPieceType() == PieceType.QUEEN && !kingSafe) {
                            counter -= 900;
                        }
                    }
                }
            }
        }
        return counter;
    }

    private int rateMovability(int listlength, int depth, int material, Move move) {
        int counter = 0;
        counter += listlength;

        if(listlength == 0) {
            if(!kingSafe(move)) {
                counter -= 200000*depth;
            } else {
                counter -= 150000*depth;
            }
        }

        return counter;
    }

    private boolean kingSafe(Move move) {
        if(isValidMove(move)) {
            return false;
        }
        return true;
    }

    private Square squareOfKing() {
        Square[][] squares = board.getBoard();
        Square squareOfKing = null;

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Square square = squares[i][j];
                if(square.isOccupied()) {
                    if(square.getPiece().getPieceType() == PieceType.KING) {
                        if(square.getPiece().getAlliance() == alliance) {
                            squareOfKing = square;
                        }
                    }
                }
            }
        }

        return squareOfKing;
    }

    //-------------------------------------------------------------------------------------------------------//
}