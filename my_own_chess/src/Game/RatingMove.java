package Game;

public class RatingMove {
    private final int rating;
    private final Move move;

    public RatingMove(int rating, Move move) {
        this.rating = rating;
        this.move = move;
    }

    public int getRating() {
        return rating;
    }

    public Move getMove() {
        return move;
    }
}
